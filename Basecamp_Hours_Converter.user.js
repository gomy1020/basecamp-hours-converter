// ==UserScript==
// @name        Basecamp Hours Converter
// @namespace   toish
// @include     https://citrusmedia.basecamphq.com/projects/*/time_entries
// @version     1
// @grant       none
// ==/UserScript==

var text = [
	"Gimme da hours boss",
	"Hours plz",
	"Beep Boop",
	"Hey, wan' sum decimals?",
	"I'm having the time of my life.",
	"You checked off the milestone?",
	"minutes sux, bruh",
	"get sum hours m8",
	"I can't think of time puns",
	"&#9664; Is that right?",
	"<em>Puzzle:</em> Z2V0IGJhY2sgdG8gd29yaw==",
	"ʞɹoʍ oʇ ʞɔɐq ʇǝb",
	"Travaillez maintenant s'il vous plait"
];

text.push( "There are " + ( text.length + 1 ) + " lame jokes" );

var timeCell  = document.querySelector("td.hours");
var timeInput = document.querySelector("td.hours input");
var btn       = document.createElement("div");

btn.innerHTML = text[ Math.floor(Math.random() * text.length) ];

style  = "background-color: rgb(162, 162, 162);";
style += "color: #FFF;";
style += "padding: 3px 9px 0px 9px;";
style += "display: inline-block;";
style += "font-size: 12px;";
style += "font-family: \"Lucida Grande\",verdana,arial,helvetica,sans-serif;";
style += "-webkit-font-smoothing: antialiased;-moz-osx-font-smoothing: grayscale;";
style += "vertical-align: top;";
style += "cursor: pointer;";
style += "box-sizing: border-box;";
style += "margin-top: 1px;"
style += "height: " + (timeInput.offsetHeight - 2) + "px;";

btn.style = style;

btn.onclick = function() {
	var mins        = parseInt(timeInput.value);
	var hours       = ( mins / 60 );
	hours           = Math.round( hours * 100 ) / 100;
	timeInput.value = hours;
}

timeCell.appendChild(btn);